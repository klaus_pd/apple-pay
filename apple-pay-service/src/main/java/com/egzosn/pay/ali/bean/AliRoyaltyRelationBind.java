package com.egzosn.pay.ali.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.egzosn.pay.common.bean.Order;
import com.egzosn.pay.common.util.str.StringUtils;

public class AliRoyaltyRelationBind implements Order {

	private static final long serialVersionUID = 1L;
	
	private String outRequestNo;
	private String type = "userId"; // 分账方类型
	private String account; // 分账方帐号
	private String name; // 分账方全称
	private String memo; // 分账关系描述

	/**
	 * 订单附加信息，可用于预设未提供的参数，这里会覆盖以上所有的订单信息，
	 */
	private volatile Map<String, Object> attr;

	public AliRoyaltyRelationBind() {
	}

	@Override
	public Map<String, Object> getAttrs() {
		if (null == attr) {
			attr = new HashMap<>();
		}
		return attr;
	}

	@Override
	public Object getAttr(String key) {
		return getAttrs().get(key);
	}

	/**
	 * 添加订单信息
	 * 
	 * @param key   key
	 * @param value 值
	 */
	@Override
	public void addAttr(String key, Object value) {
		getAttrs().put(key, value);
	}

	public String getOutRequestNo() {
		return outRequestNo;
	}

	public void setOutRequestNo(String outRequestNo) {
		this.outRequestNo = outRequestNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Map<String, Object> getAttr() {
		return attr;
	}

	public void setAttr(Map<String, Object> attr) {
		this.attr = attr;
	}
	
	/**
     * 请求参数的集合，最大长度不限，除公共参数外所有请求参数都必须放在这个参数中传递，具体参照各产品快速接入文档
     * @return 请求参数的集合
     */
    public Map<String, Object> toBizContent(){
        Map<String, Object> bizContent = new TreeMap<String, Object>();
        bizContent.put("out_request_no", outRequestNo);
        bizContent.put("receiver_list", toRoyaltyParameters());
        return bizContent;
    }
    
    /**
     * 获取分账明细信息
     * @return 分账明细信息
     */
    public List<Map<String, Object>> toRoyaltyParameters(){
    	List<Map<String, Object>> receiverList = new ArrayList<>();
        Map<String, Object> receiver = new TreeMap<String, Object>();

        if (StringUtils.isNotEmpty(type)){
        	receiver.put("type",  type);
        }

        if (StringUtils.isNotEmpty(account)){
        	receiver.put("account",  account);
        }
        if (StringUtils.isNotEmpty(name)){
        	receiver.put("name",  name);
        }
        if (StringUtils.isNotEmpty(memo)){
        	receiver.put("memo",   memo);
        }
        receiverList.add(receiver);
        return receiverList;
    }

}

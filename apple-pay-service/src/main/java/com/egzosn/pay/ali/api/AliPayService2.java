package com.egzosn.pay.ali.api;

import static com.egzosn.pay.ali.bean.AliPayConst.BIZ_CONTENT;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.egzosn.pay.ali.bean.AliRoyaltyRelationBind;
import com.egzosn.pay.ali.bean.AliTransactionType;
import com.egzosn.pay.ali.bean.OrderSettle2;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.common.http.UriVariables;

/**
 * 微信支付服务
 *
 * @author egan
 * <pre>
 * email egzosn@gmail.com
 * date 2016-5-18 14:09:01
 * </pre>
 */
public class AliPayService2 extends AliPayService implements AliRoyaltyRelationService {

    private static final Logger LOG = LoggerFactory.getLogger(AliPayService2.class);

	public AliPayService2(AliPayConfigStorage payConfigStorage) {
		super(payConfigStorage);
	}
	
	/**
     * 创建支付服务
     *
     * @param payConfigStorage 微信对应的支付配置
     * @param configStorage    微信对应的网络配置，包含代理配置、ssl证书配置
     */
    public AliPayService2(AliPayConfigStorage payConfigStorage, HttpConfigStorage configStorage) {
        super(payConfigStorage, configStorage);
    }

	@Override
	public Map<String, Object> bind(AliRoyaltyRelationBind order) {
        //获取公共参数
        Map<String, Object> parameters = getPublicParameters(AliRoyaltyRelationType.BIND);
        setAppAuthToken(parameters, order.getAttrs());
        final Map<String, Object> bizContent = order.toBizContent();
        bizContent.putAll(order.getAttrs());
        parameters.put(BIZ_CONTENT, JSON.toJSONString(bizContent));
        //设置签名
        setSign(parameters);
        return getHttpRequestTemplate().postForObject(getReqUrl() + "?" + UriVariables.getMapToParameters(parameters), null, JSONObject.class);
	}
	
	/**
     * 统一收单交易结算接口
     *
     * @param order 交易结算信息
     * @return 结算结果
     */
    public Map<String, Object> settle2(OrderSettle2 order) {
        //获取公共参数
        Map<String, Object> parameters = getPublicParameters(AliTransactionType.SETTLE);
        setAppAuthToken(parameters, order.getAttrs());
        final Map<String, Object> bizContent = order.toBizContent();
        bizContent.putAll(order.getAttrs());
        parameters.put(BIZ_CONTENT, JSON.toJSONString(bizContent));
        //设置签名
        setSign(parameters);
        LOG.info(parameters.toString());
        return getHttpRequestTemplate().postForObject(getReqUrl() + "?" + UriVariables.getMapToParameters(parameters), null, JSONObject.class);
    }

}

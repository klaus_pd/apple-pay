package com.egzosn.pay.ali.bean;

public class AliResult {

	private String msg;
	private String code;
	private String result_code;

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

	public String getResult_code() {
		return result_code;
	}
}

package com.egzosn.pay.ali.api;

import com.egzosn.pay.common.bean.TransactionType;

public enum AliRoyaltyRelationType implements TransactionType {
	
	BIND("alipay.trade.royalty.relation.bind");

	AliRoyaltyRelationType(String method) {
		this.method = method;
	}

	private String method;

	@Override
	public String getType() {
		return this.name();
	}

	@Override
	public String getMethod() {
		return this.method;
	}

}

package com.egzosn.pay.ali.api;

import java.util.Map;

import com.egzosn.pay.ali.bean.AliRoyaltyRelationBind;

public interface AliRoyaltyRelationService {

	public Map<String, Object> bind(AliRoyaltyRelationBind relationBind);
}

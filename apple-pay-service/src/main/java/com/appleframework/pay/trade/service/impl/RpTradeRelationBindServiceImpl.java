package com.appleframework.pay.trade.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.support.json.JSONUtils;
import com.appleframework.config.core.PropertyConfigurer;
import com.appleframework.pay.common.core.enums.PayWayEnum;
import com.appleframework.pay.common.core.enums.PublicStatusEnum;
import com.appleframework.pay.trade.dao.RpTradeRelationBindDao;
import com.appleframework.pay.trade.entity.RpTradeRelationBind;
import com.appleframework.pay.trade.entity.RpUserPayOauth;
import com.appleframework.pay.trade.exception.TradeBizException;
import com.appleframework.pay.trade.service.RpTradeRelationBindService;
import com.appleframework.pay.trade.service.RpUserPayOauthService;
import com.appleframework.pay.trade.utils.MD5Util;
import com.appleframework.pay.user.entity.RpUserPayInfo;
import com.appleframework.pay.user.service.RpUserPayInfoService;
import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.ali.api.AliPayService2;
import com.egzosn.pay.ali.bean.AliRoyaltyRelationBind;
import com.egzosn.pay.common.bean.CertStoreType;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.wx.api.WxConst;
import com.egzosn.pay.wx.api.WxPayConfigStorage;
import com.egzosn.pay.wx.api.WxPayService2;
import com.egzosn.pay.wx.bean.WxAddReceiver;

@Service("rpTradeRelationBindService")
public class RpTradeRelationBindServiceImpl implements RpTradeRelationBindService {
	
    private static final Logger LOG = LoggerFactory.getLogger(RpTradeRelationBindServiceImpl.class);

    @Autowired
    private RpUserPayInfoService rpUserPayInfoService;
    
	@Autowired
	private RpUserPayOauthService rpUserPayOauthService;

	@Resource
	private RpTradeRelationBindDao rpTradeRelationBindDao;
	
	@Override
	public RpTradeRelationBind getByMD5(String md5) {
		return rpTradeRelationBindDao.selectByMD5(md5);
	}

	@Override
	public void add(RpTradeRelationBind record) {
		record.setStatus(PublicStatusEnum.ACTIVE.name());
		rpTradeRelationBindDao.insert(record);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void bind(String spUserNo, String subUserNo, String psAccount, String psName,PayWayEnum payWay) {
        
		RpUserPayInfo spUserPayInfo = rpUserPayInfoService.getByUserNo(spUserNo, payWay.name());
		RpUserPayInfo subUserPayInfo = rpUserPayInfoService.getByUserNo(subUserNo, payWay.name());

		String appid = spUserPayInfo.getAppId();
		String mch_id = spUserPayInfo.getMerchantId();
		
		String md5 = this.getMD5(payWay.name(), spUserNo, subUserNo, psAccount);
		if(this.isExist(md5) ) {
			//throw new TradeBizException(TradeBizException.TRADE_PRSH_ERROR, "分账帐号已经添加"); 
			return;
		}

        if (PayWayEnum.WEIXIN.equals(payWay)){//微信支付
        	
			String sub_mch_id = null;
			String partnerKey = spUserPayInfo.getPartnerKey();
			
    		if(null != subUserPayInfo) {
    			if(null != subUserPayInfo.getMerchantId()) {
    				sub_mch_id = subUserPayInfo.getMerchantId();
    			}
    		}
			        	
        	WxPayConfigStorage wxPayConfigStorage = new WxPayConfigStorage();
    		wxPayConfigStorage.setMchId(mch_id);
    		wxPayConfigStorage.setAppId(appid);
    		//wxPayConfigStorage.setKeyPublic("转账公钥，转账时必填");
    		//wxPayConfigStorage.setSecretKey(partnerKey);
    		wxPayConfigStorage.setKeyPrivate(partnerKey);
    		wxPayConfigStorage.setNotifyUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
    		wxPayConfigStorage.setReturnUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
    		wxPayConfigStorage.setSignType(WxConst.HMACSHA256);
    		wxPayConfigStorage.setInputCharset("utf-8");
    		
    	    HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
    	    httpConfigStorage.setKeystore(spUserPayInfo.getRsaPrivateKey());
    	    httpConfigStorage.setStorePassword(mch_id);
    	    
            httpConfigStorage.setCertStoreType(CertStoreType.PATH);
            
            if(null != sub_mch_id) {
    			wxPayConfigStorage.setSubMchId(sub_mch_id);    			
    		}
    		
	        //支付服务
    		WxPayService2 service =  new WxPayService2(wxPayConfigStorage, httpConfigStorage);
	        
			// 支付订单基础信息
			//String subject, String body, BigDecimal price, String outTradeNo
	        WxAddReceiver addReceiver = new WxAddReceiver();
	        addReceiver.setAccount(psAccount);
	        addReceiver.setName(psName);
	        addReceiver.setType("MERCHANT_ID");
	        addReceiver.setRelationType("SERVICE_PROVIDER");
	        Map<String, Object> result = service.addReceiver(addReceiver);
	        
	        if(success.equals(result.get("return_code")) && success.equals(result.get("return_code"))) {
		        RpTradeRelationBind receiver = new RpTradeRelationBind();
		        receiver.setUserNo(spUserPayInfo.getUserNo());
		        receiver.setUserName(spUserPayInfo.getUserName());
		        receiver.setSubUserNo(subUserPayInfo.getUserNo());
		        receiver.setSubUserName(subUserPayInfo.getUserName());

		        receiver.setPayWayCode(payWay.name());
		        receiver.setPayWayName(payWay.getDesc());
		        
		        receiver.setPsAccount(psAccount);
		        receiver.setPsRelationType("SERVICE_PROVIDER");
		        receiver.setPsType("MERCHANT_ID");
		        receiver.setPsName(psName);
		        receiver.setContentMd5(md5);
		        
		        this.add(receiver);

	        } else {
	        	throw new TradeBizException(TradeBizException.TRADE_PRSH_ERROR, result.get("msg") + ""); 
			}

        } else {
			//支付宝
        	
        	AliPayConfigStorage aliPayConfigStorage = new AliPayConfigStorage();
            aliPayConfigStorage.setPid(mch_id);
            aliPayConfigStorage.setAppId(appid);
//          aliPayConfigStorage.setAppAuthToken("ISV代商户代用，指定appAuthToken");
            aliPayConfigStorage.setKeyPublic(spUserPayInfo.getRsaPublicKey());
            aliPayConfigStorage.setKeyPrivate(spUserPayInfo.getRsaPrivateKey());
            aliPayConfigStorage.setNotifyUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
            aliPayConfigStorage.setReturnUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
            aliPayConfigStorage.setSignType("RSA2");
            
            aliPayConfigStorage.setInputCharset("utf-8");
            aliPayConfigStorage.setTest(false);
            
            String seller_id = spUserPayInfo.getMerchantId();
			
			if(null != subUserPayInfo) {
				seller_id = subUserPayInfo.getMerchantId();
			}
			
			aliPayConfigStorage.setSeller(seller_id);
			
			//aliPayConfigStorage.setSeller(seller_id);
			
			RpUserPayOauth oauth = rpUserPayOauthService.getOne("ALIPAY", appid, mch_id, seller_id);
			if (null != oauth) {
				String authToken = oauth.getAuthToken();
				if (StringUtils.isEmpty(authToken)) {
					LOG.error("alipay_支付宝获取authToken失败，sellerId=" + seller_id);
				}
				aliPayConfigStorage.setAppAuthToken(authToken.trim());
			}
			
			//支付服务
    		AliPayService2 service =  new AliPayService2(aliPayConfigStorage);
	        
			// 支付订单基础信息
			//String subject, String body, BigDecimal price, String outTradeNo
    		AliRoyaltyRelationBind addReceiver = new AliRoyaltyRelationBind();
	        addReceiver.setAccount(psAccount);
	        addReceiver.setName(psName);
	        addReceiver.setMemo("SERVICE_PROVIDER");
	        addReceiver.setOutRequestNo(System.currentTimeMillis() + "");
	        Map<String, Object> result = service.bind(addReceiver);
	        String json = result.get("alipay_trade_royalty_relation_bind_response") + "";
	        Map<String, Object> aliResult = (Map<String, Object>)JSONUtils.parse(json);
	        if (null != aliResult && aliResult.get("code").equals("10000")) {
	        	
	        	RpTradeRelationBind receiver = new RpTradeRelationBind();
		        receiver.setUserNo(spUserPayInfo.getUserNo());
		        receiver.setUserName(spUserPayInfo.getUserName());
		        receiver.setSubUserNo(subUserPayInfo.getUserNo());
		        receiver.setSubUserName(subUserPayInfo.getUserName());
		        
		        receiver.setPayWayCode(payWay.name());
		        receiver.setPayWayName(payWay.getDesc());
		        
		        receiver.setPsAccount(psAccount);
		        receiver.setPsRelationType("SERVICE_PROVIDER");
		        receiver.setPsType("MERCHANT_ID");
		        receiver.setPsName(psName);
		        receiver.setContentMd5(md5);
	        	this.add(receiver);
			} else {
	        	throw new TradeBizException(TradeBizException.TRADE_PRSH_ERROR, aliResult.get("sub_msg") + ""); 
			}
		}
	}
	
	private String success = "SUCCESS";
	
	private boolean isExist(String md5) {
		if(null == this.getByMD5(md5)) {
			return false;
		}
		return true;
	}
	
	private String getMD5(String payWayCode, String merchantNo, String subMerchantNo, String account) {
		String key = payWayCode + merchantNo + subMerchantNo + subMerchantNo;
		return MD5Util.encode(key);
	}

}

package com.appleframework.pay.trade.dao;

import com.appleframework.pay.common.core.dao.BaseDao;
import com.appleframework.pay.trade.entity.RpTradeRelationBind;

public interface RpTradeRelationBindDao extends BaseDao<RpTradeRelationBind> {

	public RpTradeRelationBind selectByMD5(String md5);

}

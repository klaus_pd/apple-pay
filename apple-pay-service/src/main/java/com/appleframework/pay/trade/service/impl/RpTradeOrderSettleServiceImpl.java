package com.appleframework.pay.trade.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.support.json.JSONUtils;
import com.appleframework.config.core.PropertyConfigurer;
import com.appleframework.pay.account.service.RpAccountTransactionService;
import com.appleframework.pay.common.core.enums.PayWayEnum;
import com.appleframework.pay.trade.dao.RpTradePaymentOrderDao;
import com.appleframework.pay.trade.dao.RpTradePaymentRecordDao;
import com.appleframework.pay.trade.entity.RpTradePaymentOrder;
import com.appleframework.pay.trade.entity.RpTradePaymentRecord;
import com.appleframework.pay.trade.entity.RpUserPayOauth;
import com.appleframework.pay.trade.enums.OrderFromEnum;
import com.appleframework.pay.trade.enums.TradeStatusEnum;
import com.appleframework.pay.trade.enums.TrxTypeEnum;
import com.appleframework.pay.trade.exception.TradeBizException;
import com.appleframework.pay.trade.service.RpTradeOrderSettleService;
import com.appleframework.pay.trade.service.RpTradeRelationBindService;
import com.appleframework.pay.trade.service.RpUserPayOauthService;
import com.appleframework.pay.user.entity.RpUserPayConfig;
import com.appleframework.pay.user.entity.RpUserPayInfo;
import com.appleframework.pay.user.exception.UserBizException;
import com.appleframework.pay.user.service.BuildNoService;
import com.appleframework.pay.user.service.RpUserPayConfigService;
import com.appleframework.pay.user.service.RpUserPayInfoService;
import com.egzosn.pay.ali.api.AliPayConfigStorage;
import com.egzosn.pay.ali.api.AliPayService2;
import com.egzosn.pay.ali.bean.OrderSettle2;
import com.egzosn.pay.common.bean.CertStoreType;
import com.egzosn.pay.common.exception.PayErrorException;
import com.egzosn.pay.common.http.HttpConfigStorage;
import com.egzosn.pay.wx.api.WxConst;
import com.egzosn.pay.wx.api.WxPayConfigStorage;
import com.egzosn.pay.wx.api.WxPayService2;
import com.egzosn.pay.wx.bean.WxProfitSharing;

@Service("rpTradeOrderSettleService")
public class RpTradeOrderSettleServiceImpl implements RpTradeOrderSettleService {
	
    private static final Logger LOG = LoggerFactory.getLogger(RpTradeOrderSettleServiceImpl.class);

    @Autowired
    private RpTradePaymentOrderDao rpTradePaymentOrderDao;
    
    @Autowired
    private RpTradePaymentRecordDao rpTradePaymentRecordDao;

    @Autowired
    private RpUserPayConfigService rpUserPayConfigService;

    @Autowired
    private RpUserPayInfoService rpUserPayInfoService;
    
    @Autowired
    private BuildNoService buildNoService;
    
    @Autowired
    private RpTradeRelationBindService rpTradeRelationBindService;
    
    @Autowired
    private RpAccountTransactionService rpAccountTransactionService;
    
	@Autowired
	private RpUserPayOauthService rpUserPayOauthService;
    
	@Override
	public void doSharing(String payKey, String merchantOrderNo, String outOrderNo, BigDecimal amount, String orderIp) {
		
		RpUserPayConfig spUserPayConfig = rpUserPayConfigService.getByPayKey(payKey);
        if (spUserPayConfig == null){
            throw new UserBizException(UserBizException.USER_PAY_CONFIG_ERRPR,"用户支付配置有误");
        }
		
        String spMerchantNo = spUserPayConfig.getUserNo();//商户编号
        RpTradePaymentOrder subTradePaymentOrder = rpTradePaymentOrderDao.selectBySpMerchantNoAndMerchantOrderNo(spMerchantNo, merchantOrderNo);

		if (subTradePaymentOrder == null) {
			throw new TradeBizException(TradeBizException.TRADE_ORDER_ERROR, "订单不存在,不能分账");
		}
        
        RpTradePaymentRecord subTradePaymentRecord = rpTradePaymentRecordDao.getSuccessRecordBySpMerchantNoAndMerchantOrderNo(spMerchantNo, merchantOrderNo);
		if (subTradePaymentRecord == null) {
			throw new TradeBizException(TradeBizException.TRADE_ORDER_ERROR, "交易记录不存在或未支付成功,不能分账");
		}
        
		BigDecimal totalFee = subTradePaymentOrder.getOrderAmount();

		if (totalFee.subtract(amount).doubleValue() < 0) {
			throw new TradeBizException(TradeBizException.TRADE_ORDER_ERROR, "TradeBizException");
		}
        
		String payWayCode = subTradePaymentRecord.getPayWayCode();// 支付方式
		
		// 根据资金流向获取配置信息
		RpUserPayInfo spUserPayInfo = rpUserPayInfoService.getByUserNo(subTradePaymentOrder.getSpMerchantNo(), payWayCode);
		RpUserPayInfo subUserPayInfo = rpUserPayInfoService.getByUserNo(subTradePaymentOrder.getMerchantNo(), payWayCode);

		if (spUserPayInfo == null) {
			throw new UserBizException(UserBizException.USER_IS_NULL, "服务商支付配置不存在");
		}
		if (subUserPayInfo == null) {
			throw new UserBizException(UserBizException.USER_IS_NULL, "子商户支付配置不存在");
		}

		RpTradePaymentOrder rpTradePaymentOrder = sealRpTradePaymentOrder(spUserPayInfo, subTradePaymentOrder, outOrderNo, amount, orderIp);
		try {
			rpTradePaymentOrderDao.insert(rpTradePaymentOrder);
		} catch (Exception e) {
			throw new TradeBizException(TradeBizException.TRADE_PRSH_ERROR, "outOrderNo参数不能重复");
		}

		RpTradePaymentRecord rpTradePaymentRecord = sealRpTradePaymentRecord(spUserPayInfo, subTradePaymentRecord, outOrderNo, amount, orderIp);
		rpTradePaymentRecordDao.insert(rpTradePaymentRecord);
		
		try {
			rpTradeRelationBindService.bind(subTradePaymentOrder.getSpMerchantNo(), subTradePaymentOrder.getMerchantNo(),
					spUserPayInfo.getMerchantId(), spUserPayConfig.getRemark(), PayWayEnum.valueOf(payWayCode));
		} catch (TradeBizException e) {
			LOG.info(e.getMessage());
			throw e;
		}

		try {
			Map<String, Object> result = this.doProfitsharing(spUserPayInfo, subUserPayInfo, subTradePaymentRecord, amount);
			String bankTrxNo = result.get("transaction_id").toString();
			completeSuccessOrder(rpTradePaymentRecord, rpTradePaymentOrder, bankTrxNo, new Date(), JSONUtils.toJSONString(result));
		} catch (PayErrorException e) {
			completeFailOrder(rpTradePaymentRecord, rpTradePaymentOrder, e.getPayError().getString());
			throw e;
		}
        
	}
	
	/**
     * 支付成功方法
     * @param rpTradePaymentRecord
     */
	private void completeSuccessOrder(RpTradePaymentRecord rpTradePaymentRecord, RpTradePaymentOrder rpTradePaymentOrder,
			String bankTrxNo, Date timeEnd, String bankReturnMsg) {

		LOG.info("completeSuccessOrder:rpTradePaymentRecord:" + rpTradePaymentRecord);
		LOG.info("completeSuccessOrder:bankTrxNo:" + bankTrxNo);
		LOG.info("completeSuccessOrder:timeEnd:" + timeEnd);
		LOG.info("completeSuccessOrder:bankReturnMsg:" + bankReturnMsg);
		rpTradePaymentRecord.setPaySuccessTime(timeEnd);
		rpTradePaymentRecord.setBankTrxNo(bankTrxNo);// 设置银行流水号
		rpTradePaymentRecord.setBankReturnMsg(bankReturnMsg);
		rpTradePaymentRecord.setStatus(TradeStatusEnum.SUCCESS.name());
		rpTradePaymentRecordDao.update(rpTradePaymentRecord);

		rpTradePaymentOrder.setStatus(TradeStatusEnum.SUCCESS.name());
		rpTradePaymentOrder.setTrxNo(rpTradePaymentRecord.getTrxNo());// 设置支付平台支付流水号
		rpTradePaymentOrderDao.update(rpTradePaymentOrder);

		LOG.info("completeSuccessOrder:rpTradePaymentOrder:" + rpTradePaymentOrder);
		LOG.info("completeSuccessOrder:TrxNo:" + rpTradePaymentRecord.getTrxNo());

		// if (FundInfoTypeEnum.PLAT_RECEIVES.name().equals(rpTradePaymentRecord.getFundIntoType())) {
		rpAccountTransactionService.creditToAccount(rpTradePaymentRecord.getMerchantNo(),
				rpTradePaymentRecord.getOrderAmount().subtract(rpTradePaymentRecord.getPlatIncome()),
				rpTradePaymentRecord.getBankOrderNo(), rpTradePaymentRecord.getBankTrxNo(),
				rpTradePaymentRecord.getTrxType(), rpTradePaymentRecord.getRemark());
		// }
	}
	
	/**
     * 支付失败方法
     * @param rpTradePaymentRecord
     */
    private void completeFailOrder(RpTradePaymentRecord rpTradePaymentRecord, RpTradePaymentOrder rpTradePaymentOrder, 
    		String bankReturnMsg){
        rpTradePaymentRecord.setBankReturnMsg(bankReturnMsg);
        rpTradePaymentRecord.setStatus(TradeStatusEnum.FAILED.name());
        rpTradePaymentRecordDao.update(rpTradePaymentRecord);

        rpTradePaymentOrder.setStatus(TradeStatusEnum.FAILED.name());
        rpTradePaymentOrderDao.update(rpTradePaymentOrder);

    }
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> doProfitsharing(RpUserPayInfo spUserPayInfo, RpUserPayInfo subUserPayInfo, 
			RpTradePaymentRecord subTradePaymentRecord, BigDecimal amount) {
		String appid = spUserPayInfo.getAppId();
		String mch_id = spUserPayInfo.getMerchantId();
		String partnerKey = spUserPayInfo.getPartnerKey();
		
		String payWayCode = spUserPayInfo.getPayWayCode();
		
		if (PayWayEnum.WEIXIN.name().equals(payWayCode)) {// 微信支付
			        	
			WxPayConfigStorage wxPayConfigStorage = new WxPayConfigStorage();
			wxPayConfigStorage.setMchId(mch_id);
			wxPayConfigStorage.setAppId(appid);
			//wxPayConfigStorage.setKeyPublic("转账公钥，转账时必填");
			//wxPayConfigStorage.setSecretKey(partnerKey);
			wxPayConfigStorage.setKeyPrivate(partnerKey);
			wxPayConfigStorage.setNotifyUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
			wxPayConfigStorage.setReturnUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
			wxPayConfigStorage.setSignType(WxConst.HMACSHA256);
			wxPayConfigStorage.setInputCharset("utf-8");
			
		    HttpConfigStorage httpConfigStorage = new HttpConfigStorage();
		    httpConfigStorage.setKeystore(spUserPayInfo.getRsaPrivateKey());
		    httpConfigStorage.setStorePassword(mch_id);
		    
	        httpConfigStorage.setCertStoreType(CertStoreType.PATH);
			
			if(null != subUserPayInfo) {
				if(null != subUserPayInfo.getMerchantId()) {
					wxPayConfigStorage.setSubMchId(subUserPayInfo.getMerchantId());
				}
			}
			
	        //支付服务
			WxPayService2 service =  new WxPayService2(wxPayConfigStorage, httpConfigStorage);
	        
			// 支付订单基础信息
			//String subject, String body, BigDecimal price, String outTradeNo
	        WxProfitSharing profitSharing = new WxProfitSharing();
	        profitSharing.setOutOrderNo(subTradePaymentRecord.getBankOrderNo());
	        profitSharing.setTransactionId(subTradePaymentRecord.getBankTrxNo());
	        profitSharing.setAccount(mch_id);
	        profitSharing.setAmount(amount);
	        profitSharing.setType("MERCHANT_ID");
	        profitSharing.setDescription("嘚啷分账");
	        
		    return service.doProfitsharing(profitSharing);

        } else if (PayWayEnum.ALIPAY.name().equals(payWayCode)) {
			
            AliPayConfigStorage aliPayConfigStorage = new AliPayConfigStorage();
            aliPayConfigStorage.setPid(mch_id);
            aliPayConfigStorage.setAppId(appid);
//          aliPayConfigStorage.setAppAuthToken("ISV代商户代用，指定appAuthToken");
            aliPayConfigStorage.setKeyPublic(spUserPayInfo.getRsaPublicKey());
            aliPayConfigStorage.setKeyPrivate(spUserPayInfo.getRsaPrivateKey());
            aliPayConfigStorage.setNotifyUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
            aliPayConfigStorage.setReturnUrl(PropertyConfigurer.getString("weixinpay.notify_url"));
            aliPayConfigStorage.setSignType("RSA2");
            
            aliPayConfigStorage.setInputCharset("utf-8");
            aliPayConfigStorage.setTest(false);
            
            String transOut = null;
            
            String seller_id = spUserPayInfo.getMerchantId();
			
			if(null != subUserPayInfo) {
				seller_id = subUserPayInfo.getMerchantId();
				transOut = subUserPayInfo.getMerchantId();
			}
			
			//aliPayConfigStorage.setSeller(seller_id);
			
			RpUserPayOauth oauth = rpUserPayOauthService.getOne("ALIPAY", appid, mch_id, seller_id);
			if (null != oauth) {
				String authToken = oauth.getAuthToken();
				if (StringUtils.isEmpty(authToken)) {
					LOG.error("alipay_支付宝获取authToken失败，sellerId=" + seller_id);
					return null;
				}
				aliPayConfigStorage.setAppAuthToken(authToken.trim());
			}
            
            //支付服务
            AliPayService2 service = new AliPayService2(aliPayConfigStorage);

            OrderSettle2 order = new OrderSettle2();
            order.setTradeNo(subTradePaymentRecord.getBankTrxNo());
            order.setOutRequestNo(subTradePaymentRecord.getBankOrderNo());
            order.setTransOut(transOut);
            order.setAmount(amount);
            order.setTransIn(mch_id);
            
            Map<String, Object> result = service.settle2(order);
            LOG.info(result.toString());
	        String json = result.get("alipay_trade_order_settle_response") + "";
	        Map<String, Object> aliResult = (Map<String, Object>)JSONUtils.parse(json);
	        if (null != aliResult && aliResult.get("code").equals("10000")) {
	        	result.put("transaction_id", aliResult.get("trade_no") + "");
	        } else {
				throw new TradeBizException(TradeBizException.TRADE_ORDER_ERROR, aliResult.get("sub_msg") + "");
			}
            return result;
		} else {
			return null;
		}
    	
	}
	
	/**
     * 分账订单实体封装
     * @return
     */
	private RpTradePaymentOrder sealRpTradePaymentOrder(RpUserPayInfo spUserPayInfo, RpTradePaymentOrder subTradePaymentOrder,
			String outOrderNo, BigDecimal orderAmount, String orderIp) {
		Date now = new Date();
		RpTradePaymentOrder rpTradePaymentOrder = new RpTradePaymentOrder();

		rpTradePaymentOrder.setProductName("分账");// 商品名称
		rpTradePaymentOrder.setMerchantOrderNo(outOrderNo); // 分账原始订单号
		rpTradePaymentOrder.setOrderAmount(orderAmount); // 分账金额

		rpTradePaymentOrder.setMerchantName(spUserPayInfo.getUserName());// 商户名称
		rpTradePaymentOrder.setMerchantNo(spUserPayInfo.getUserNo());// 商户编号
		rpTradePaymentOrder.setOrderDate(now);// 下单日期
		rpTradePaymentOrder.setOrderTime(now);// 下单时间
		rpTradePaymentOrder.setOrderIp(orderIp);// 下单IP
		rpTradePaymentOrder.setOrderRefererUrl("");// 下单前页面
		rpTradePaymentOrder.setReturnUrl("");// 页面通知地址
		rpTradePaymentOrder.setNotifyUrl("");// 后台通知地址
		rpTradePaymentOrder.setOrderPeriod(5);// 订单有效期

		rpTradePaymentOrder.setExpireTime(new Date(System.currentTimeMillis() + 5 * 60));// 订单过期时间
		rpTradePaymentOrder.setPayWayCode(subTradePaymentOrder.getPayWayCode());// 支付通道编码
		rpTradePaymentOrder.setPayWayName(subTradePaymentOrder.getPayWayName());// 支付通道名称
		rpTradePaymentOrder.setStatus(TradeStatusEnum.WAITING_PAYMENT.name());// 订单状态 等待支付

		rpTradePaymentOrder.setPayTypeCode(subTradePaymentOrder.getPayTypeCode());// 支付类型
		rpTradePaymentOrder.setPayTypeName(subTradePaymentOrder.getPayTypeName());// 支付方式

		rpTradePaymentOrder.setFundIntoType("MERCHANT_RECEIVES");// 资金流入方向

		rpTradePaymentOrder.setRemark(subTradePaymentOrder.getRemark());// 支付备注
		// rpTradePaymentOrder.setField1(subTradePaymentOrder.getField1());//扩展字段1
		// rpTradePaymentOrder.setField2(subTradePaymentOrder.getField2());//扩展字段2
		// rpTradePaymentOrder.setField3(subTradePaymentOrder.getField3());//扩展字段3
		// rpTradePaymentOrder.setField4(subTradePaymentOrder.getField4());//扩展字段4
		// rpTradePaymentOrder.setField5(subTradePaymentOrder.getField5());//扩展字段5

		// rpTradePaymentOrder.setSpMerchantNo(spMerchantNo);

		return rpTradePaymentOrder;
	}

	private RpTradePaymentRecord sealRpTradePaymentRecord(
			RpUserPayInfo spUserPayInfo, RpTradePaymentRecord subTradePaymentRecord, 
			String outOrderNo, BigDecimal orderAmount, String orderIp) {
		RpTradePaymentRecord rpTradePaymentRecord = new RpTradePaymentRecord();
		rpTradePaymentRecord.setProductName("分账");// 产品名称
		rpTradePaymentRecord.setMerchantOrderNo(outOrderNo);// 分账原始订单号

		String trxNo = buildNoService.buildTrxNo();
		rpTradePaymentRecord.setTrxNo(trxNo);// 支付流水号

		String bankOrderNo = buildNoService.buildBankOrderNo();
		rpTradePaymentRecord.setBankOrderNo(bankOrderNo);// 银行订单号
		rpTradePaymentRecord.setMerchantName(spUserPayInfo.getUserName());
		rpTradePaymentRecord.setMerchantNo(spUserPayInfo.getUserNo());// 商户编号
		rpTradePaymentRecord.setOrderIp(orderIp);// 下单IP
		rpTradePaymentRecord.setOrderRefererUrl("");// 下单前页面
		rpTradePaymentRecord.setReturnUrl("");// 页面通知地址
		rpTradePaymentRecord.setNotifyUrl("");// 后台通知地址
		rpTradePaymentRecord.setPayWayCode(subTradePaymentRecord.getPayWayCode());// 支付通道编码
		rpTradePaymentRecord.setPayWayName(subTradePaymentRecord.getPayWayName());// 支付通道名称
		rpTradePaymentRecord.setTrxType(TrxTypeEnum.SHARING.name());// 交易类型
		rpTradePaymentRecord.setOrderFrom(OrderFromEnum.MERCHANT_SHARING.name());// 订单来源
		rpTradePaymentRecord.setOrderAmount(orderAmount);// 订单金额
		rpTradePaymentRecord.setStatus(TradeStatusEnum.WAITING_PAYMENT.name());// 订单状态 等待支付

		rpTradePaymentRecord.setPayTypeCode(subTradePaymentRecord.getPayTypeCode());// 支付类型
		rpTradePaymentRecord.setPayTypeName(subTradePaymentRecord.getPayTypeName());// 支付方式
		rpTradePaymentRecord.setFundIntoType("MERCHANT_RECEIVES");// 资金流入方向

		return rpTradePaymentRecord;
	}
		


}

package com.appleframework.pay.oauth.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AppTokenExchangeSubElement;
import com.alipay.api.request.AlipayOpenAuthTokenAppRequest;
import com.alipay.api.response.AlipayOpenAuthTokenAppResponse;
import com.appleframework.pay.oauth.service.OauthTokenAlipayService;
import com.appleframework.pay.trade.model.UserPayOauthBo;
import com.appleframework.pay.trade.service.RpUserPayOauthService;
import com.appleframework.pay.user.entity.RpUserPayInfo;
import com.appleframework.pay.user.service.RpUserPayInfoService;

@Service
public class OauthTokenAlipayServiceImpl implements OauthTokenAlipayService {

    private static final Logger LOG = LoggerFactory.getLogger(OauthTokenAlipayServiceImpl.class);

    @Autowired
    private RpUserPayInfoService rpUserPayInfoService;
    
    @Autowired
    private RpUserPayOauthService rpUserPayOauthService;
    
    private String alipayServerUrl = "https://openapi.alipay.com/gateway.do";
    
	@Override
	public String completeGetOauthToken(String payWayCode, Map<String, String> notifyMap) {
    	//第三方授权返回 {app_auth_code=b5d217604ee040c1996948822741eF84, source=alipay_app_auth, app_id=2018061560378496}

    	String appAuthCode = notifyMap.get("app_auth_code");
    	String appId = notifyMap.get("app_id");
    	String userNo = notifyMap.get("state"); //state传输的为userNo
    	if(null == userNo) {
    		userNo = "delangjuhe";
    	}

        RpUserPayInfo rpUserPayInfo = rpUserPayInfoService.getByUserNo(userNo, payWayCode);
        String mchPrivateKey = rpUserPayInfo.getRsaPrivateKey();
        String mchPublicKey = rpUserPayInfo.getRsaPublicKey();
        
        AlipayClient alipayClient = new DefaultAlipayClient(alipayServerUrl, 
        		rpUserPayInfo.getAppId(), mchPrivateKey, "json", "utf-8", mchPublicKey, "RSA2");

    	AlipayOpenAuthTokenAppRequest request = new AlipayOpenAuthTokenAppRequest();
    	String bizContent = JSON.toJSONString(new GrantAuthorization(appAuthCode));
    	request.setBizContent(bizContent);
    	try {
			AlipayOpenAuthTokenAppResponse response = alipayClient.execute(request);
			LOG.info("AlipayOpenAuthTokenAppResponse = " + JSON.toJSONString(response));
			String code = response.getCode();
			if(null != code && code.equals("10000")) {
				AppTokenExchangeSubElement token = response.getTokens().get(0);
				UserPayOauthBo userPayOauthBo = new UserPayOauthBo();
				userPayOauthBo.setAppId(appId);
				userPayOauthBo.setAppType("ALIPAY");
				userPayOauthBo.setAuthToken(token.getAppAuthToken());
				userPayOauthBo.setRefreshToken(token.getAppRefreshToken());
				userPayOauthBo.setExpiresIn(Integer.parseInt(token.getExpiresIn()));
				userPayOauthBo.setReExpiresIn(Integer.parseInt(token.getReExpiresIn()));
				userPayOauthBo.setPayUserid(token.getUserId());
				userPayOauthBo.setUserNo(userNo);
				userPayOauthBo.setMerchantId(rpUserPayInfo.getMerchantId());
				LOG.info("UserPayOauthBo=" + JSON.toJSONString(userPayOauthBo));
				rpUserPayOauthService.saveOrUpdate(userPayOauthBo);
			}
			else {
				
			}
			
			System.out.println(response);
		} catch (AlipayApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	class GrantAuthorization {
		
		private String grant_type = "authorization_code";
		private String code;

		public String getGrant_type() {
			return grant_type;
		}

		public void setGrant_type(String grant_type) {
			this.grant_type = grant_type;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public GrantAuthorization() {}
		
		public GrantAuthorization(String code) {
			super();
			this.code = code;
		}

	}
	
}

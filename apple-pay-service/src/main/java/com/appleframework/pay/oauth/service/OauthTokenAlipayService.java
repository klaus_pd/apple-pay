package com.appleframework.pay.oauth.service;

import java.util.Map;

public interface OauthTokenAlipayService {

    /**
     * 完成第三方绑定
     *
     * @param payWayCode
     * @param notifyMap
     * @return
     */
    public String completeGetOauthToken(String payWayCode, Map<String, String> notifyMap);

}
